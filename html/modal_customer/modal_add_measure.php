
<div id="addMeasuresModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
            <form name="add_measure" id="add_measure">
					<div class="modal-header">						
						<h4 class="modal-title">Add Measure</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					

						<div class="form-group">
							<label>Neck</label>
							<input type="text" name="neck" id="neck" class="form-control" required>
                        </div>
                        <div class="form-group">
							<label>Chest</label>
							<input type="text" name="chest" id="chest" class="form-control" required>
                        </div>
                        <div class="form-group">
							<label>Waist</label>
							<input type="text" name="waist" id="waist" class="form-control" required>
                        </div>
                        <div class="form-group">
							<label>Forearm</label>
							<input type="text" name="forearm" id="forearm" class="form-control" required>
						</div>
                        <div class="form-group">
							<label>Thigh</label>
							<input type="text" name="thigh" id="thigh" class="form-control" required>
                        </div>
                        <div class="form-group">
							<label>Biceps</label>
							<input type="text" name="biceps" id="biceps" class="form-control" required>
                        </div>
                        <div class="form-group">
							<label>Id Customer</label>
							<input type="text" name="id_customer" id="id_customer" class="form-control" required>
						</div>
						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" id="submit_btn" class="btn btn-success" value="Save data">
					</div>
				</form>

			</div>
		</div>
</div>

