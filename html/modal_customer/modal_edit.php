<div id="editCustomerModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="edit_customer" id="edit_customer">
					<div class="modal-header">						
						<h4 class="modal-title">Edit Customer</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">	
						
						<div class="form-group">
							<input type="hidden" name="edit_code" id="edit_code" class="form-control" required>
						</div>
						<div class="form-group">
							<input type="hidden" name="edit_ci_customer" id="edit_ci_customer" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="edit_name" id="edit_name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Surnames</label>
							<input type="text" name="edit_surnames" id="edit_surnames" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="edit_email" id="edit_email" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="edit_password" id="edit_password" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" name="edit_confirm_password" id="edit_confirm_password" class="form-control" required>
						</div>
						<div>
							<label>Type User</label>
							<select name="edit_type_user" id="edit_type_user" class="form-control">
								<option value="Customer">Customer</option>
							</select>
							<span class="help-block"></span>
						</div>
						<div class="form-group">
							<label>Id Plan</label>
							<input type="text" name="edit_id_plan" id="edit_id_plan" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Id Locker</label>
							<input type="text" name="edit_id_locker" id="edit_id_locker" class="form-control" required>
						</div>
						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-info" value="Save data">
					</div>
				</form>
			</div>
		</div>
	</div>