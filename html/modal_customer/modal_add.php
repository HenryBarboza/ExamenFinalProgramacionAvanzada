
<div id="addCustomerModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="form-register" role="form" id="register-form">
					<div class="modal-header">						
						<h4 class="modal-title">Add Customer</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>CI Customer</label>
							<input type="text" name="ci_customer"  id="ci_customer" class="form-control" required>
							
						</div>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Surnames</label>
							<input type="text" name="surnames" id="surnames" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email" id="email" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" id="password" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" name="confirm_password" id="confirm_password" class="form-control" required>
						</div>
						<div>
							<label>Type User</label>
							<select name="type_user" id="type_user" class="form-control">
								<option value="Administrator">Customer</option>
							</select>
							<span class="help-block"></span>
						</div>
						<div class="form-group">
							<label>Id Plan</label>
							<input type="text" name="id_plan" id="id_plan" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Id Locker</label>
							<input type="text" name="id_locker" id="id_locker" class="form-control" required>
						</div>

						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" id="submit_btn" class="btn btn-success" value="Save data">
					</div>
				</form>

			</div>
		</div>
</div>


