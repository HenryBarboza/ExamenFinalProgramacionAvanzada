
<div id="addCoachModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
            <form name="add_coach" id="add_coach">
					<div class="modal-header">						
						<h4 class="modal-title">Add Coach</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					

						<div class="form-group">
							<label>CI Coach</label>
							<input type="text" name="ci_coach" id="ci_coach" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Surnames</label>
							<input type="text" name="surnames" id="surnames" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="text" name="phone" id="phone" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Address</label>
							<input type="text" name="address" id="address" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Profession</label>
							<input type="text" name="profession" id="profession" class="form-control" required>
						</div>

						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" id="submit_btn" class="btn btn-success" value="Save data">
					</div>
				</form>

			</div>
		</div>
</div>


