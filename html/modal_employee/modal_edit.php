
<div id="editEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
            <form name="edit_employee" id="edit_employee">
					<div class="modal-header">						
						<h4 class="modal-title">Edit Employee</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">	
						<div class="form-group">
							<input type="hidden" name="edit_id_employee" id="edit_id_employee" class="form-control" required>
						</div>				
						<div class="form-group">
							<label>CI Employee</label>
							<input type="text" name="edit_ci_employee" id="edit_ci_employee" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="edit_name" id="edit_name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Surnames</label>
							<input type="text" name="edit_surnames" id="edit_surnames" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="text" name="edit_phone" id="edit_phone" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Address</label>
							<input type="text" name="edit_address" id="edit_address" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Position</label>
							<input type="text" name="edit_position" id="edit_position" class="form-control" required>
						</div>

						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" id="submit_btn" class="btn btn-success" value="Save data">
					</div>
				</form>

			</div>
		</div>
</div>


