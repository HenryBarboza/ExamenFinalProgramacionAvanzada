
<div id="addPlanModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="form-register" role="form" id="register-form">
					<div class="modal-header">						
						<h4 class="modal-title">Add Plan</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					

						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Price</label>
							<input type="text" name="price" id="price" class="form-control" required>
						</div>

						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" id="submit_btn" class="btn btn-success" value="Save data">
					</div>
				</form>

			</div>
		</div>
</div>


