<div id="editLockerModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="edit_locker" id="edit_locker">
					<div class="modal-header">						
						<h4 class="modal-title">Edit Locker</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<input type="hidden" name="edit_code"  id="edit_code" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Nro Locker</label>
							<input type="text" name="edit_nro_locker" id="edit_nro_locker" class="form-control" required>
						</div>
						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-info" value="Save data">
					</div>
				</form>
			</div>
		</div>
	</div>