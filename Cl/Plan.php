<?php
class Cl_Plan
{
	/** Variable que va a contener la conexión de base de datos **/
	protected $_con;
	
	/** Inicializar DBclass **/
	public function __construct()
	{
		$db = new Cl_DBclass();
		$this->_con = $db->con;
	}

	/** METODO DE REGISTRO **/
	public function registration( array $data )
	{
		if( !empty( $data ) ){
			
			// Trim todos los datos entrantes
			$trimmed_data = array_map('trim', $data);
			// trim —> Elimina espacio en blanco (u otro tipo de caracteres) del inicio y el final de la cadena
			

			// Escapar de las variables para la seguridad
			// Escapa los caracteres especiales de una cadena para usarla en una sentencia SQL, tomando en cuenta el conjunto de caracteres actual de la conexión
			$name = mysqli_real_escape_string( $this->_con, $trimmed_data['name'] );
			$price = mysqli_real_escape_string( $this->_con, $trimmed_data['price'] );
			
			
			if((!$name) || (!$price) ) {
				throw new Exception( FIELDS_MISSING );
			}

			$query = "INSERT INTO plan ( id_plan, name, price ) VALUES (NULL, '$name', '$price')";

			if(mysqli_query($this -> _con, $query)){
				mysqli_close($this -> _con);
				return true;
			};
		} else{
			throw new Exception( USER_REGISTRATION_FAIL );
		}
	}
}