<?php
class Cl_Customer
{
	/** Variable que va a contener la conexión de base de datos **/
	protected $_con;
	
	/** Inicializar DBclass **/
	public function __construct()
	{
		$db = new Cl_DBclass();
		$this->_con = $db->con;
	}

	/** METODO DE REGISTRO DE USUARIOS **/
	public function registration( array $data )
	{
		if( !empty( $data ) ){
			
			// Trim todos los datos entrantes
			$trimmed_data = array_map('trim', $data);
			// trim —> Elimina espacio en blanco (u otro tipo de caracteres) del inicio y el final de la cadena
			

			// Escapar de las variables para la seguridad
			// Escapa los caracteres especiales de una cadena para usarla en una sentencia SQL, tomando en cuenta el conjunto de caracteres actual de la conexión
			$ci_customer = mysqli_real_escape_string( $this->_con, $trimmed_data['ci_customer'] );
			$name = mysqli_real_escape_string( $this->_con, $trimmed_data['name'] );
			$surnames = mysqli_real_escape_string( $this->_con, $trimmed_data['surnames'] );
			$password = mysqli_real_escape_string( $this->_con, $trimmed_data['password'] );
			$cpassword = mysqli_real_escape_string( $this->_con, $trimmed_data['confirm_password'] );
			$type_user = mysqli_real_escape_string( $this ->_con, $trimmed_data['type_user']);
			$id_plan = mysqli_real_escape_string( $this ->_con, $trimmed_data['id_plan']);
			$id_locker = mysqli_real_escape_string( $this ->_con, $trimmed_data['id_locker']);
			
			
			// Verifica la direccion de correo electrónico
			if (filter_var( $trimmed_data['email'], FILTER_VALIDATE_EMAIL)) {
				$email = mysqli_real_escape_string( $this->_con, $trimmed_data['email']);
			} else {
				throw new Exception( "Please enter a valid email address!" );
			}
			
			
			if((!$ci_customer) ||(!$name) || (!$surnames) || (!$email) || (!$type_user) || (!$password) || (!$cpassword) || (!$id_plan) || (!$id_locker)  ) {
				throw new Exception( FIELDS_MISSING );
			}
			if ($password !== $cpassword) {
				throw new Exception( PASSWORD_NOT_MATCH );
			}

			$password = md5( $password );
			$query = "INSERT INTO customer (id_customer, ci_customer, name, surnames, email, password, type_user, id_plan, id_locker) VALUES (NULL, '$ci_customer', '$name', '$surnames',  '$email', '$password', '$type_user', '$id_plan', '$id_locker')";

			if(mysqli_query($this -> _con, $query)){
				mysqli_close($this -> _con);
				return true;
			};
		} else{
			throw new Exception( USER_REGISTRATION_FAIL );
		}
	}

}