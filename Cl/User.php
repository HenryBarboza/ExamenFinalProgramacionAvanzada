<?php
class Cl_User
{
	/** Variable que va a contener la conexión de base de datos **/
	protected $_con;
	
	/** Inicializar DBclass **/
	public function __construct()
	{
		$db = new Cl_DBclass();
		$this->_con = $db->con;
	}

	/** METODO DE REGISTRO DE USUARIOS **/
	public function registration( array $data )
	{
		if( !empty( $data ) ){
			
			// Trim todos los datos entrantes
			$trimmed_data = array_map('trim', $data);
			// trim —> Elimina espacio en blanco (u otro tipo de caracteres) del inicio y el final de la cadena
			

			// Escapar de las variables para la seguridad
			// Escapa los caracteres especiales de una cadena para usarla en una sentencia SQL, tomando en cuenta el conjunto de caracteres actual de la conexión
			$name = mysqli_real_escape_string( $this->_con, $trimmed_data['name'] );
			$password = mysqli_real_escape_string( $this->_con, $trimmed_data['password'] );
			$cpassword = mysqli_real_escape_string( $this->_con, $trimmed_data['confirm_password'] );
			$type_user = mysqli_real_escape_string( $this ->_con, $trimmed_data['type_user']);
			
			
			// Verifica la direccion de correo electrónico
			if (filter_var( $trimmed_data['email'], FILTER_VALIDATE_EMAIL)) {
				$email = mysqli_real_escape_string( $this->_con, $trimmed_data['email']);
			} else {
				throw new Exception( "Please enter a valid email address!" );
			}
			
			
			if((!$name) || (!$email) || (!$type_user) || (!$password) || (!$cpassword) ) {
				throw new Exception( FIELDS_MISSING );
			}
			if ($password !== $cpassword) {
				throw new Exception( PASSWORD_NOT_MATCH );
			}

			$password = md5( $password );
			$query = "INSERT INTO users (id_user, name, email, password, type_user, created) VALUES (NULL, '$name', '$email', '$password', '$type_user', CURRENT_TIMESTAMP)";

			if(mysqli_query($this -> _con, $query)){
				mysqli_close($this -> _con);
				return true;
			};
		} else{
			throw new Exception( USER_REGISTRATION_FAIL );
		}
	}

	/** METODO PARA INICIAR SESION **/
	public function login( array $data )
	{
		$_SESSION['logged_in'] = false;
		$_SESSION['user'] = "";

		if( !empty( $data ) ){
			
			// Trim todos los datos entrantes:
			$trimmed_data = array_map('trim', $data);
			
			// escapar de las variables para la seguridad
			$email = mysqli_real_escape_string( $this->_con,  $trimmed_data['email'] );
			$password = mysqli_real_escape_string( $this->_con,  $trimmed_data['password'] );

			$type_user = mysqli_real_escape_string( $this->_con,  $trimmed_data['type_user'] );
				
			if((!$email) || (!$password) ) {
				throw new Exception( LOGIN_FIELDS_MISSING );
			}

			//$password = md5( $password );

			$query = "SELECT id_user, name, email FROM users where email = '$email' and type_user = '$type_user' and password = '$password' ";

			$result = mysqli_query($this->_con, $query);
			$data = mysqli_fetch_assoc($result);
			$count = mysqli_num_rows($result);
			mysqli_close($this->_con);

			if( $count == 1){
				$_SESSION = $data;
				$_SESSION['logged_in'] = true;
				$_SESSION['user'] = $type_user;
				return true;
			}else{
				throw new Exception( LOGIN_FAIL );
			}
		} else{
			throw new Exception( LOGIN_FIELDS_MISSING );
		}
	}
	

	/** METODO PARA CAMBIO DE CONTRASEÑA **/
	public function account( array $data )
	{
		if( !empty( $data ) ){
			// Trim todos los datos entrantes
			$trimmed_data = array_map('trim', $data);
			
			// Escapar de las variables para la seguridad
			$password = mysqli_real_escape_string( $this->_con, $trimmed_data['password'] );
			$cpassword = $trimmed_data['confirm_password'];
			$id_user = mysqli_real_escape_string( $this->_con, $trimmed_data['id_user'] );
			
			if((!$password) || (!$cpassword) ) {
				throw new Exception( FIELDS_MISSING );
			}
			if ($password !== $cpassword) {
				throw new Exception( PASSWORD_NOT_MATCH );
			}
			$password = md5( $password );
			$query = "UPDATE users SET password = '$password' WHERE id_user = '$id_user'";
			if(mysqli_query($this->_con, $query)){
				mysqli_close($this->_con);
				return true;
			}
		} else{
			throw new Exception( FIELDS_MISSING );
		}
	}
	
	/** METODO PARA CERRAR SESION **/
	public function logout()
	{
		session_unset();
		session_destroy();
		header('Location: index.php');
	}
	
	
	/* * GENERA CONTRASEÑA ALEATORIA * */
	private function randomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); 
		$alphaLength = strlen($alphabet) - 1; // Se pone la longitud -1 en caché
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //convertir el array en una cadena
	}
}