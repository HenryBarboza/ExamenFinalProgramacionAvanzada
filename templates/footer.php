<div class="clearfix"></div>
	<!-- Footer -->
    <footer>
    	<div class="container-fluid">
    		<p class="text-center">Copyright by <a href="https://www.facebook.com/henry.barbozasuarez" target="_blank">Henry Barboza Suarez</a> <?php echo date("Y")?></p>
    	</div>
    </footer>
    <!-- /Footer -->
  </body>
</html>
<?php ob_end_flush(); ?>