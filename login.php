<?php 
ob_start();
session_start();
require_once 'config.php'; 
?>
<?php 
	
	if( !empty( $_POST )){
		try {
			$user_obj = new Cl_User();
			$data = $user_obj->login( $_POST );
			if(isset( $_SESSION['logged_in']) && $_SESSION['logged_in'] && $_SESSION['user'] == "Administrator" ){
				header('Location: customer.php');
			}else if(isset( $_SESSION['logged_in']) && $_SESSION['logged_in'] && $_SESSION['user'] == "Customer" ){
				header('Location: customer_profile.php');
			}
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
	}

	//print_r($_SESSION);
	if(isset( $_SESSION['logged_in']) && $_SESSION['logged_in'] && $_SESSION['user'] == "Administrator" ){
		header('Location: customer.php');
	}else if(isset( $_SESSION['logged_in']) && $_SESSION['logged_in'] && $_SESSION['user'] == "Student" ){
		header('Location: customer_profile.php');
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Página de inicio de sesión</title>
	
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>

	<div class="container">
		<div class="login-form">
			<?php require_once 'templates/message.php';?>
			
			<div class="form-header">
				<i class="fa fa-user"></i>
			</div>
			<form id="login-form" method="post" class="form-signin" role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				<input name="email" id="email" type="email" class="form-control" placeholder="Email" autofocus> 
				<select name="type_user" id="type_user" class="form-control">
					<option value="Administrator">Administrator</option>
					<option value="Student">Customer</option>
					<option value="Student">Employee</option>
				</select>
				<input name="password" id="password" type="password" class="form-control" placeholder="Password">
				
				<button class="btn btn-block bt-login" type="submit" id="submit_btn" data-loading-text="Login....">Login</button>
				<a href="index.php" class="btn btn-block bt-login">To Return</a>
			</form>
			<div class="form-footer">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<i class="fa fa-lock"></i>
						<a href="#"> Did you forget your password? </a>
					
					</div>
					
					<div class="col-xs-6 col-sm-6 col-md-6">
						<i class="fa fa-check"></i>
						<a href="#"> Sign in</a>
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="js/jquery.validate.min.js"></script>
    <script src="js/login.js"></script>
  </body>
</html>
<?php ob_end_flush(); ?>
