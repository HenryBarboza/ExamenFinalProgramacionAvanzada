<?php
require_once 'messages.php';
 
define( 'BASE_PATH', 'http://localhost/login/');//Ruta base donde se encuentra
define( 'DB_HOST', 'localhost' );//Servidor de la base de datos
define( 'DB_USERNAME', 'root');//Usuario de la base de datos
define( 'DB_PASSWORD', '');//Contraseña de la base de datos
define( 'DB_NAME', 'henryfit');//Nombre de la base de datos
 
//Intenta cargar una clase sin definir
function __autoload($class)
{
	$parts = explode('_', $class); //Divide un string en varios string
	$path = implode(DIRECTORY_SEPARATOR,$parts); //Une elementos de un array en un string
	//DIRECTORY_SEPARATOR -> Para que las aplicaciones PHP que utilizan rutas de archivos funcionaran correctamente y sin hacer cambios tanto en Windows como en Linux.
	require_once $path . '.php';
}