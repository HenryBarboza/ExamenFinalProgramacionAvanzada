<?php require_once 'templates/header.php';?>
<?php require_once 'config.php'; ?>
<?php 
	if(!empty($_POST)){
		try {
			$user_obj = new Cl_Customer();
			$data = $user_obj->registration( $_POST );
			if($data)$success = USER_REGISTRATION_SUCCESS;
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
	}
?>

        <section class="banner-customer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1> <b>"NEW WEEK <span class="span">NEW GOALS"</b> </span></h1>
                        <p>Look in the mirror, that's your competition</p>
                    </div>
            </div>
        </div>
        </section>

     <div class="container course">
        <div class="table-wrapper">

            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h3>MANAGE  <b>CUSTOMERS</b> </h3>
					</div>
					
                </div>
            </div>
            
            <?php require_once 'templates/message.php';?>
			
			<div class="col-sm-8">
						<a href="#addCustomerModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add new customer</span></a>
			</div>

			<div class='col-sm-4 pull-right'>
				<div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control" placeholder="Search customer"  id="q" onkeyup="load(1);" />
                                <span class="input-group-btn">
                                    <button class="btn btn-info" type="button" onclick="load(1);">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                </div>
			</div>

			<div class='clearfix'></div>
            <hr>
            
			<div id="loader_customers"></div>
			<div id="resultados"></div>
            <div class='outer_div_customers'></div>

            <div class="table-sub-title">
                <div class="row">
                    <div class="col-sm-6">
						<h3>BODY MEASURES</h3>
					</div>
					
                </div>
            </div>

             <div class="col-sm-12">
				<a href="#addMeasuresModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add a new Body Measures</span></a>
			</div>
		
            <div class='clearfix'></div>
            <hr>
            <div id="loader_measures"></div>
			<div id="resultados"></div>
            <div class='outer_div_measures'></div>
            
			
        </div>
	</div>

	<?php include("html/modal_customer/modal_add.php");?>
	<?php include("html/modal_customer/modal_edit.php");?>
    <?php include("html/modal_customer/modal_delete.php");?>

    <?php include("html/modal_customer/modal_add_measure.php");?>
	<?php include("html/modal_customer/modal_edit_measure.php");?>
    <?php include("html/modal_customer/modal_delete_measure.php");?>

	<script src="js/customer_script.js"></script>
    

	
<?php require_once 'templates/footer.php';?>
