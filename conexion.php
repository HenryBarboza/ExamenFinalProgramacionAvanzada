<?php

	define('DB_HOST','localhost');
	define('DB_USER','root');
	define('DB_PASS','');
	define('DB_NAME','henryfit');

	$con=@mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
    if(!$con){
        die("Impossible to connect: ".mysqli_error($con));
    }
    if (@mysqli_connect_errno()) {
        die("The connection failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
    }
?>