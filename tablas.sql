--  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,

CREATE DATABASE IF NOT EXISTS `henryfit` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `henryfit`;

CREATE TABLE IF NOT EXISTS `plan`
(
    `id_plan` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    `price` FLOAT NOT NULL,
    PRIMARY KEY(`id_plan`),
    UNIQUE `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `locker`
(
    `id_locker` INT NOT NULL AUTO_INCREMENT,
    `nro_locker` INT NOT NULL,
    PRIMARY KEY(`id_locker`),
    UNIQUE `nro_locker` (`nro_locker`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ci_customer` INT NOT NULL,
  `name` varchar(50) NOT NULL,
  `surnames` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `type_user` varchar(100) NOT NULL,
  `id_plan` INT NOT NULL,
  `id_locker` INT NOT NULL,

  PRIMARY KEY (`id_customer`),
  UNIQUE `ci_customer` (`ci_customer`),
  UNIQUE `email` (`email`),
  KEY `password` (`password`),


  INDEX (`id_plan`),
  FOREIGN KEY (`id_plan`) REFERENCES plan(`id_plan`),
  INDEX (`id_locker`),
  FOREIGN KEY (`id_locker`) REFERENCES locker(`id_locker`)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `type_user` varchar(100) NOT NULL,

  PRIMARY KEY (`id_user`),
  KEY `email` (`email`),
  KEY `login` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `measure` (
  `id_measure` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `neck` FLOAT NOT NULL,
  `chest` FLOAT NOT NULL,
  `waist` FLOAT NOT NULL,
  `forearm` FLOAT NOT NULL,
  `thigh` FLOAT NOT NULL,
  `biceps` FLOAT NOT NULL,
  `id_customer` INT NOT NULL,

  PRIMARY KEY (`id_measure`)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `employee` (
  `id_employee` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ci_employee` INT NOT NULL,
  `name` varchar(50) NOT NULL,
  `surnames` varchar(60) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `address` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,

  PRIMARY KEY (`id_employee`),
  UNIQUE(`ci_employee`)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `coach` (
  `id_coach` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ci_coach` INT NOT NULL,
  `name` varchar(50) NOT NULL,
  `surnames` varchar(60) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `address` varchar(100) NOT NULL,
  `profession` varchar(60) NOT NULL,

  PRIMARY KEY (`id_coach`),
  UNIQUE(`ci_coach`)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;