<?php
	if (empty($_POST['name'])){
		$errors[] = "Enter the name.";
	} elseif (!empty($_POST['name'])){

	require_once ("../conexion.php");

    $ci_coach = mysqli_real_escape_string($con,(strip_tags($_POST["ci_coach"],ENT_QUOTES)));
    $name= mysqli_real_escape_string($con,(strip_tags($_POST["name"],ENT_QUOTES)));
    $surnames = mysqli_real_escape_string($con,(strip_tags($_POST["surnames"],ENT_QUOTES)));
    $phone = mysqli_real_escape_string($con,(strip_tags($_POST["phone"],ENT_QUOTES)));
    $address = mysqli_real_escape_string($con,(strip_tags($_POST["address"],ENT_QUOTES)));
    $profession = mysqli_real_escape_string($con,(strip_tags($_POST["profession"],ENT_QUOTES)));

 

    $sql = "INSERT INTO coach(id_coach, ci_coach, name, surnames, phone, address, profession) VALUES (NULL,'$ci_coach', '$name', '$surnames', '$phone', '$address', '$profession')";
    $query = mysqli_query($con,$sql);


    if ($query) {
        $messages[] = "The measure has been saved successfully.";
    } else {
        $errors[] = "Sorry, the registration failed. Please, come back and try again.";
    }
		
	} else 
	{
		$errors[] = "Unknown.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Well Done!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>