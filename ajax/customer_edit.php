<?php
	if (empty($_POST['edit_code'])){
		$errors[] = "ID está vacío.";
	} elseif (!empty($_POST['edit_code'])){

	require_once ("../conexion.php");

	$id_customer = mysqli_real_escape_string($con,(strip_tags($_POST["edit_code"],ENT_QUOTES)));
	$ci_customer = mysqli_real_escape_string($con,(strip_tags($_POST["edit_ci_customer"],ENT_QUOTES)));
	$name = mysqli_real_escape_string($con,(strip_tags($_POST["edit_name"],ENT_QUOTES)));
	$surnames= mysqli_real_escape_string($con,(strip_tags($_POST["edit_surnames"],ENT_QUOTES)));
	$email= mysqli_real_escape_string($con,(strip_tags($_POST["edit_email"],ENT_QUOTES)));
	$password =mysqli_real_escape_string($con,(strip_tags($_POST['edit_password'],ENT_QUOTES)));
	$cpassword = mysqli_real_escape_string($con,(strip_tags($_POST['edit_confirm_password'],ENT_QUOTES)));
	$type_user= mysqli_real_escape_string($con,(strip_tags($_POST['edit_type_user'],ENT_QUOTES)));
	$id_plan = mysqli_real_escape_string($con,(strip_tags($_POST['edit_id_plan'],ENT_QUOTES)));
	$id_locker = mysqli_real_escape_string($con,(strip_tags($_POST['edit_id_locker'],ENT_QUOTES)));

	
	if((!$name) || (!$email) || (!$type_user) || (!$password) || (!$cpassword) ) {
		$messages[] = "Sorry, the update failed. Please, come back and try again.";
	}
	if ($password !== $cpassword) {
		$errors[]  = "Sorry, the update failed. Please, come back and try again.";
	}

	$sql = "UPDATE customer SET id_customer ='".$id_customer."', ci_customer='".$ci_customer."', name ='".$name."', surnames ='".$surnames."', email ='".$email."', password ='".$password."', type_user ='".$type_user."', id_plan ='".$id_plan."', id_locker ='".$id_locker."' WHERE id_customer ='".$id_customer."' ";
	
    $query = mysqli_query($con,$sql);

    if ($query) {
        $messages[] = "The course has been updated successfully.";
    } else {
        $errors[] = "Sorry, the update failed. Please, come back and try again.";
    }
		
	} else 
	{
		$errors[] = "Unknown.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Well Done!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>