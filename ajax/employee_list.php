<?php
	
	require_once ("../conexion.php");
 

	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	
	if($action == 'ajax'){

        $query = mysqli_real_escape_string($con,(strip_tags($_REQUEST['query'], ENT_QUOTES)));
        
		$tables="employee";
		$campos="*";
		$sWhere="employee.name LIKE '%".$query."%'";
		$sWhere.="order by employee.name";
		
		
		include 'pagination.php'; // Include pagination file
		// Pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = intval($_REQUEST['per_page']); //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con,"SELECT count(*) AS numrows FROM $tables where $sWhere ");
		if ($row= mysqli_fetch_array($count_query)){$numrows = $row['numrows'];}
		else {echo mysqli_error($con);}
		$total_pages = ceil($numrows/$per_page);


	
		$query = mysqli_query($con,"SELECT $campos FROM  $tables where $sWhere LIMIT $offset, $per_page");

		
	
	
		
		if ($numrows>0){
			
		?>
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class='text-center'>IdEmployee</th>
							<th class='text-center'>CIEmployee</th>
                            <th class='text-center'>Name</th>
                            <th class='text-center'>Surnames</th>
                            <th class='text-center'>Phone</th>
                            <th class='text-center'>Address</th>
                            <th class='text-center'>Position</th>
							<th></th>
						</tr>
					</thead>
					<tbody>	
							<?php 
							$finales=0;
							while($row = mysqli_fetch_array($query)){	
								$id_employee=$row['id_employee'];
								$ci_employee=$row['ci_employee'];
                                $name=$row['name'];
                                $surnames=$row['surnames'];
                                $phone=$row['phone'];
                                $address=$row['address'];
                                $position=$row['position'];
						
								$finales++;
								?>	
								<tr class="<?php echo $text_class;?>" >
									<td class='text-center'><?php echo $id_employee;?></td>
									<td class='text-center'><?php echo $ci_employee;?></td>
                                    <td class='text-center'><?php echo $name;?></td>
                                    <td class='text-center'><?php echo $surnames;?></td>
                                    <td class='text-center'><?php echo $phone;?></td>
                                    <td class='text-center'><?php echo $address?></td>
                                    <td class='text-center'><?php echo $position;?></td>

									<td>
										<a href="#"  data-target="#editEmployeeModal" class="edit" data-toggle="modal" data-code="<?php echo $id_employee;?>" data-ci="<?php echo $ci_employee;?>" data-name="<?php echo $name?>" data-surnames="<?php echo $surnames?>" data-phone="<?php echo $phone;?>" data-address="<?php echo $address;?>" data-position="<?php echo $position;?>"><i class="material-icons" data-toggle="tooltip" title="Edit" >&#xE254;</i></a>
									
										<a href="#deleteEmployeeModal" class="delete" data-toggle="modal" data-id="<?php echo $id_employee;?>"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
									</td>
								</tr>
							<?php }?>
							<tr>
								<td colspan='6'> 
									<?php 
										$inicios=$offset+1;
										$finales+=$inicios -1;
										echo "Showing from $inicios to $finales of $numrows records";
										echo paginate( $page, $total_pages, $adjacents);
									?>
								</td>
							</tr>
					</tbody>			
				</table>
			</div>	
		<?php	
		}
	}
?>          