<?php
	if (empty($_POST['neck'])){
		$errors[] = "Enter the measures.";
	} elseif (!empty($_POST['neck'])){

	require_once ("../conexion.php");

    $neck = mysqli_real_escape_string($con,(strip_tags($_POST["neck"],ENT_QUOTES)));
    $chest = mysqli_real_escape_string($con,(strip_tags($_POST["chest"],ENT_QUOTES)));
    $waist= mysqli_real_escape_string($con,(strip_tags($_POST["waist"],ENT_QUOTES)));
    $forearm = mysqli_real_escape_string($con,(strip_tags($_POST["forearm"],ENT_QUOTES)));
    $thigh = mysqli_real_escape_string($con,(strip_tags($_POST["thigh"],ENT_QUOTES)));
    $biceps = mysqli_real_escape_string($con,(strip_tags($_POST["biceps"],ENT_QUOTES)));
    $id_customer = mysqli_real_escape_string($con,(strip_tags($_POST["id_customer"],ENT_QUOTES)));


    $sql = "INSERT INTO measure(id_measure, neck, chest, waist, forearm, thigh, biceps, id_customer) VALUES (NULL,'$neck', '$chest', '$waist', '$forearm', '$thigh', '$biceps', '$id_customer')";
    $query = mysqli_query($con,$sql);

	
    if ($query) {
        $messages[] = "The measure has been saved successfully.";
    } else {
        $errors[] = "Sorry, the registration failed. Please, come back and try again.";
    }
		
	} else 
	{
		$errors[] = "Unknown.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Well Done!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>