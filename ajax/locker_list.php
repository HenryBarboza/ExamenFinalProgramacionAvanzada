<?php
	
	require_once ("../conexion.php");
 

	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	
	if($action == 'ajax'){

		$query = mysqli_real_escape_string($con,(strip_tags($_REQUEST['query'], ENT_QUOTES)));
	
		$tables="locker";
		$campos="*";
		$sWhere="locker.nro_locker LIKE '%".$query."%'";
		$sWhere.="order by locker.nro_locker";
		
		include 'pagination.php'; // Include pagination file
		// Pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = intval($_REQUEST['per_page']); //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con,"SELECT count(*) AS numrows FROM $tables where $sWhere ");
		if ($row= mysqli_fetch_array($count_query)){$numrows = $row['numrows'];}
		else {echo mysqli_error($con);}
		$total_pages = ceil($numrows/$per_page);


		// Main query to fetch the data
		$query = mysqli_query($con,"SELECT $campos FROM  $tables where $sWhere LIMIT $offset, $per_page");
		// Loop through fetched data
		
	
			
		if ($numrows>0){
			
		?>
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class='text-center'>IdLocker</th>
							<th class='text-center'>Nro Locker</th>
							<th></th>
						</tr>
					</thead>
					<tbody>	
							<?php 
							$finales=0;
							while($row = mysqli_fetch_array($query)){	
								$id_locker=$row['id_locker'];
								$nro_locker=$row['nro_locker'];
						
								$finales++;
								?>	
								<tr class="<?php echo $text_class;?>" >
									<td class='text-center'><?php echo $id_locker;?></td>
									<td class='text-center'><?php echo $nro_locker;?></td>


									<td>
										<a href="#"  data-target="#editLockerModal" class="edit" data-toggle="modal" data-code='<?php echo $id_locker;?>' data-nro="<?php echo $nro_locker?>"><i class="material-icons" data-toggle="tooltip" title="Edit" >&#xE254;</i></a>
										
										<a href="#deleteLockerModal" class="delete" data-toggle="modal" data-id="<?php echo $id_locker;?>"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
									</td>
								</tr>
							<?php }?>
							<tr>
								<td colspan='6'> 
									<?php 
										$inicios=$offset+1;
										$finales+=$inicios -1;
										echo "Showing from $inicios to $finales of $numrows records";
										echo paginate( $page, $total_pages, $adjacents);
									?>
								</td>
							</tr>
					</tbody>			
				</table>
			</div>	
		<?php	
		}
	}
?>          