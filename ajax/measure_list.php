<?php
	
	require_once ("../conexion.php");
 

	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	
	if($action == 'ajax'){

        $query = mysqli_real_escape_string($con,(strip_tags($_REQUEST['id_customer'], ENT_QUOTES)));
        
		$tables="measure";
		$campos="*";
		$sWhere="measure.id_customer = '".$query."'";
		
		
		include 'pagination.php'; 
		// Pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = intval($_REQUEST['per_page']); //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con,"SELECT count(*) AS numrows FROM $tables where $sWhere ");
		if ($row= mysqli_fetch_array($count_query)){$numrows = $row['numrows'];}
		else {echo mysqli_error($con);}
		$total_pages = ceil($numrows/$per_page);



		$query = mysqli_query($con,"SELECT $campos FROM  $tables where $sWhere LIMIT $offset, $per_page");

		
		if ($numrows>0){
			
		?>
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class='text-center'>IdMeasure</th>
							<th class='text-center'>Neck</th>
                            <th class='text-center'>chest</th>
                            <th class='text-center'>waist</th>
                            <th class='text-center'>Forearm</th>
                            <th class='text-center'>Thigh</th>
                            <th class='text-center'>Biceps`</th>
                            <th class='text-center'>IdCustomer</th>
							<th></th>
						</tr>
					</thead>
					<tbody>	
							<?php 
							$finales=0;
							while($row = mysqli_fetch_array($query)){	
								$id_measure=$row['id_measure'];
								$neck=$row['neck'];
                                $chest=$row['chest'];
                                $waist=$row['waist'];
                                $forearm=$row['forearm'];
                                $thigh=$row['thigh'];
                                $biceps=$row['biceps'];
                                $id_customer=$row['id_customer'];
						
								$finales++;
								?>	
								<tr class="<?php echo $text_class;?>" >
									<td class='text-center'><?php echo $id_measure;?></td>
									<td class='text-center'><?php echo $neck;?></td>
                                    <td class='text-center'><?php echo $chest;?></td>
                                    <td class='text-center'><?php echo $waist;?></td>
                                    <td class='text-center'><?php echo $forearm;?></td>
                                    <td class='text-center'><?php echo $thigh?></td>
                                    <td class='text-center'><?php echo $biceps;?></td>
                                    <td class='text-center'><?php echo $id_customer;?></td>

									<td>
										<a href="#"  data-target="#editMeasureModal" class="edit" data-toggle="modal" data-code='<?php echo $id_plan;?>' data-name="<?php echo $name?>" data-price="<?php echo $price?>"><i class="material-icons" data-toggle="tooltip" title="Edit" >&#xE254;</i></a>
										
										<a href="#deleteMeasureModal" class="delete" data-toggle="modal" data-id="<?php echo $id_measure;?>"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
									</td>
								</tr>
							<?php }?>
							<tr>
								<td colspan='6'> 
									<?php 
										$inicios=$offset+1;
										$finales+=$inicios -1;
										echo "Showing from $inicios to $finales of $numrows records";
										echo paginate( $page, $total_pages, $adjacents);
									?>
								</td>
							</tr>
					</tbody>			
				</table>
			</div>	
		<?php	
		}
	}
?>          