<?php
	if (empty($_POST['edit_id_coach'])){
		$errors[] = "ID está vacío.";
	} elseif (!empty($_POST['edit_id_coach'])){

	require_once ("../conexion.php");

	$id_coach = mysqli_real_escape_string($con,(strip_tags($_POST["edit_id_coach"],ENT_QUOTES)));
	$ci_coach = mysqli_real_escape_string($con,(strip_tags($_POST["edit_ci_coach"],ENT_QUOTES)));
	$name = mysqli_real_escape_string($con,(strip_tags($_POST["edit_name"],ENT_QUOTES)));
	$surnames= mysqli_real_escape_string($con,(strip_tags($_POST["edit_surnames"],ENT_QUOTES)));
	$phone = mysqli_real_escape_string($con,(strip_tags($_POST['edit_phone'],ENT_QUOTES)));
    $address = mysqli_real_escape_string($con,(strip_tags($_POST['edit_address'],ENT_QUOTES)));
    $profession = mysqli_real_escape_string($con,(strip_tags($_POST['edit_profession'],ENT_QUOTES)));

	
	if((!$name) || (!$surnames) || (!$phone) || (!$address) || (!$profession) ) {
		$messages[] = "Sorry, the update failed. Please, come back and try again.";
	}

	$sql = "UPDATE coach SET id_coach='".$id_coach."', ci_coach='".$ci_coach."', name='".$name."', surnames='".$surnames."', phone='".$phone."', address='".$address."', profession='".$profession."' WHERE id_coach='".$id_coach."' ";
	
    $query = mysqli_query($con,$sql);

    if ($query) {
        $messages[] = "The course has been updated successfully.";
    } else {
        $errors[] = "Sorry, the update failed. Please, come back and try again.";
    }
		
	} else 
	{
		$errors[] = "Unknown.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Well Done!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>