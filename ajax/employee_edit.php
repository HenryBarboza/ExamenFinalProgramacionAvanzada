<?php
	if (empty($_POST['edit_id_employee'])){
		$errors[] = "ID está vacío.";
	} elseif (!empty($_POST['edit_id_employee'])){

	require_once ("../conexion.php");

	$id_employee = mysqli_real_escape_string($con,(strip_tags($_POST["edit_id_employee"],ENT_QUOTES)));
	$ci_employee = mysqli_real_escape_string($con,(strip_tags($_POST["edit_ci_employee"],ENT_QUOTES)));
	$name = mysqli_real_escape_string($con,(strip_tags($_POST["edit_name"],ENT_QUOTES)));
	$surnames= mysqli_real_escape_string($con,(strip_tags($_POST["edit_surnames"],ENT_QUOTES)));
	$phone = mysqli_real_escape_string($con,(strip_tags($_POST['edit_phone'],ENT_QUOTES)));
    $address = mysqli_real_escape_string($con,(strip_tags($_POST['edit_address'],ENT_QUOTES)));
    $position = mysqli_real_escape_string($con,(strip_tags($_POST['edit_position'],ENT_QUOTES)));

	
	if((!$name) || (!$surnames) || (!$phone) || (!$address) || (!$position) ) {
		$messages[] = "Sorry, the update failed. Please, come back and try again.";
	}

	$sql = "UPDATE employee SET id_employee='".$id_employee."', ci_employee='".$ci_employee."', name='".$name."', surnames='".$surnames."', phone='".$phone."', address='".$address."', position='".$position."' WHERE id_employee='".$id_employee."' ";
	
    $query = mysqli_query($con,$sql);

    if ($query) {
        $messages[] = "The course has been updated successfully.";
    } else {
        $errors[] = "Sorry, the update failed. Please, come back and try again.";
    }
		
	} else 
	{
		$errors[] = "Unknown.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Well Done!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>