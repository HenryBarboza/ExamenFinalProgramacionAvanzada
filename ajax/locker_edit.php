<?php
	if (empty($_POST['edit_code'])){
		$errors[] = "ID está vacío.";
	} elseif (!empty($_POST['edit_code'])){

	require_once ("../conexion.php");

    $id_locker = mysqli_real_escape_string($con,(strip_tags($_POST["edit_code"],ENT_QUOTES)));
	$nro_locker= mysqli_real_escape_string($con,(strip_tags($_POST["edit_nro_locker"],ENT_QUOTES)));

	
	$id=intval($_POST['edit_code']);

    $sql = "UPDATE locker SET id_locker='".$id_locker."', nro_locker='".$nro_locker."' WHERE id_locker='".$id_locker."' ";
    $query = mysqli_query($con,$sql);

    if ($query) {
        $messages[] = "The course has been updated successfully.";
    } else {
        $errors[] = "Sorry, the update failed. Please, come back and try again.";
    }
		
	} else 
	{
		$errors[] = "Unknown.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Well Done!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>