<?php
	
	require_once ("../conexion.php");
 

	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	
	if($action == 'ajax'){

		$query = mysqli_real_escape_string($con,(strip_tags($_REQUEST['query'], ENT_QUOTES)));
	
		$tables="customer";
		$campos="*";
		$sWhere="customer.name LIKE '%".$query."%'";
		$sWhere.="order by customer.name";
		
		
		include 'pagination.php'; // Include pagination file
		// Pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = intval($_REQUEST['per_page']); //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con,"SELECT count(*) AS numrows FROM $tables where $sWhere ");
		if ($row= mysqli_fetch_array($count_query)){$numrows = $row['numrows'];}
		else {echo mysqli_error($con);}
		$total_pages = ceil($numrows/$per_page);


		// Main query to fetch the data
		$query = mysqli_query($con,"SELECT $campos FROM  $tables where $sWhere LIMIT $offset, $per_page");
		// Loop through fetched data
		
	
	
		
		if ($numrows>0){
			
		?>
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class='text-center'>IdCustomer</th>
							<th>Ci Customer</th>
							<th>Name</th>
							<th>Surnames</th>
							<th>Email</th>
							<th>Type User</th>
							<th>Plan</th>
							<th>Locker</th>
							<th></th>
						</tr>
					</thead>
					<tbody>	
							<?php 
							$finales=0;
							while($row = mysqli_fetch_array($query)){	
								$id_customer=$row['id_customer'];
								$ci_customer=$row['ci_customer'];
								$name=$row['name'];
								$surnames=$row['surnames'];
								$email=$row['email'];
								$type_user=$row['type_user'];
								$id_plan=$row['id_plan'];	
								$id_locker=$row['id_locker'];						
								$finales++;
								?>	
								<tr class="<?php echo $text_class;?>" onClick="list_measures('<?php echo $id_customer;?>')">
									<td class='text-center'><?php echo $id_customer;?></td>
									<td ><?php echo $ci_customer;?></td>
									<td ><?php echo $name;?></td>
									<td ><?php echo $surnames;?></td>
									<td ><?php echo $email;?></td>
									<td ><?php echo $type_user;?></td>
									<td ><?php echo $id_plan;?></td>
									<td ><?php echo $id_locker;?></td>

									<td>
										<a href="#"  data-target="#editCustomerModal" class="edit" data-toggle="modal" data-id="<?php echo $id_customer?>" data-ci="<?php echo $ci_customer?>"
										data-name="<?php echo $name?>" data-surnames="<?php echo $surnames?>"
										data-email="<?php echo $email?>" data-type="<?php echo $type_user?>" data-plan="<?php echo $id_plan?>"
										data-locker="<?php echo $id_locker?>"  ><i class="material-icons" data-toggle="tooltip" title="Edit" >&#xE254;</i></a>
										
										<a href="#deleteCustomerModal" class="delete" data-toggle="modal" data-id="<?php echo $id_customer;?>"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
									</td>
								</tr>
							<?php }?>
							<tr>
								<td colspan='6'> 
									<?php 
										$inicios=$offset+1;
										$finales+=$inicios -1;
										echo "Showing from $inicios to $finales of $numrows records";
										echo paginate( $page, $total_pages, $adjacents);
									?>
								</td>
							</tr>
					</tbody>			
				</table>
			</div>	
		<?php	
		}
	}
?>          