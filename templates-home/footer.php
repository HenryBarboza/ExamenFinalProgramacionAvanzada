
        <div class="clearfix"></div>

        <!-- Footer -->
          <footer>
            <div class="container-fluid">
              <p class="text-center">Copyright by <a href="https://www.facebook.com/henry.barbozasuarez" target="_blank">Henry Barboza Suarez</a> <?php echo date("Y")?></p>
            </div>
          </footer>
          <!-- /Footer -->
        

      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/fontawesome-all.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/custom.js"></script>

      <script>
      $(document).ready(function(){
        

          function sdf_FTS(_number,_decimal,_separator)
          {
          var decimal=(typeof(_decimal)!='undefined')?_decimal:2;
          var separator=(typeof(_separator)!='undefined')?_separator:'';
          var r=parseFloat(_number)
          var exp10=Math.pow(10,decimal);
          r=Math.round(r*exp10)/exp10;
          rr=Number(r).toFixed(decimal).toString().split('.');
          b=rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);
          r=(rr[1]?b+'.'+rr[1]:b);

          return r;
        }
          
        setTimeout(function(){
            $('#counter').text('0');
            $('#counter1').text('0');
            $('#counter2').text('0');
            $('#counter3').text('0');

            setInterval(function(){
              
              var curval=parseInt($('#counter').text().replace(' ',''));
              var curval1=parseInt($('#counter1').text());
              var curval2=parseInt($('#counter2').text());
              var curval3=parseInt($('#counter3').text());

              if(curval<= 10280){
                $('#counter').text(sdf_FTS((curval+20),0,' '));
              }
              if(curval1<=30){
                $('#counter1').text(curval1+1);
              }
              if(curval2<=25){
                $('#counter2').text(curval2+1);
              }
              if(curval3<=419){
                $('#counter3').text(curval3+1);
              }
            }, 2);
            
          }, 500);

      });
    </script>

  </body>
</html>

<?php ob_end_flush(); ?>