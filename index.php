<?php require_once 'templates-home/header.php';?>   

     <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active img-1">
            <div class="container">
              <div class="carousel-caption text-left">
                <h1>NO PAIN <span>NO GAIN</span> </h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. <br> Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item img-2">
            <div class="container">
              <div class="carousel-caption">
                <h1>FITNESS <span>BRING HAPPINESS</span> </h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. <br> Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item img-3">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>BEFIT <span>STRONG</span> </h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. <br> Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>


      <!--features-area start-->
      <section class="features-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="features-body">
                            <div class="features-box text-center">
                                <div class="features-elements">
                                    <a href="#"><i class="fas fa-heart icon-features"></i></a>
                                    <h4 class="mb20">bodybuilding</h4>
                                    <p class="mb20">Lorem ipsum dolor sit amet, ei veritus consetetur repudiandae eam, duo ne homero nostro moderatius.</p>
                                </div>
                                <div class="features-box-img ">
                                    <a class="primary-overlay" href="#"><img src="img/feature/1.jpg"  alt="feature img"></a>
                                </div>
                            </div>
                            <div class="features-box text-center">
                                <div class="features-elements">
                                    <a href="#"><i class="fas fa-users icon-features"></i></a>
                                    <h4 class="mb20">fitness</h4>
                                    <p class="mb20">Lorem ipsum dolor sit amet, ei veritus consetetur repudiandae eam, duo ne homero nostro moderatius.</p>
                                </div>
                                <div class="features-box-img">
                                    <a class="primary-overlay" href="#"><img src="img/feature/2.jpg"  alt="feature img"></a>
                                </div>
                            </div>
                            <div class="features-box text-center">
                                <div class="features-elements">
                                    <a href="#"><i class="fas fa-dumbbell icon-features"></i></a>
                                    <h4 class="mb20">weight left</h4>
                                    <p class="mb20">Lorem ipsum dolor sit amet, ei veritus consetetur repudiandae eam, duo ne homero nostro moderatius.</p>
                                </div>
                                <div class="features-box-img ">
                                    <a class="primary-overlay" href="#"><img src="img/feature/3.jpg"  alt="feature img"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--features-area end-->

        <!-- Home Page About Section Start Here -->
        <div class="home-about-area">
            <div class="container">
                <div class="row">
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 hidden-xs">
                         <div class="about-images">
                             <a href="#"><img src="img/about/1.png" alt=""></a>
                         </div>
                     </div>
                     <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                         <div class="about-content">
                             <div class="section-title">
                                 <h2>ABOUT <span>HENRYFIT GYM</span></h2>
                                 <hr class="hr-left text-left">
                             </div>
                             <p>Quid dubitas igitur mutare principia naturae? Negat enim summo bono afferre incrementum diem. Quamquam id quidem, infinitum est in hac urbe; Dolere malum est: in crucem qui agitur, beatus esse non potest. Duo enim genera quae erant, fecit tria. O magnam vim ingenii causamque iustam, cur nova existeret disciplina! Perge porro. Hic ambiguo ludimur. Iubet igitur nos Pythius Apollo noscere nosmet ipsos. Philosophi autem in suis lectulis plerumque moriuntur. Neque solum ea communia, verum etiam paria esse dixerunt. Quicquid porro animo cernimus, id omne oritur a sensibus.</p>
                             <p><a class="btn btn-lg btn-primary" href="#about.php" role="button">Read More</a></p>
                         </div>
                     </div>
                </div>
            </div>
        </div>
        <!-- Home Page About Section End Here -->


        <!-- Home Page Experience Section End Here -->
        <section class="experience">
    	<div class="container">
			<div class="row">
				<div class="col-md-3 experience-item">
                     <i class="fas fa-users icon-experience"></i>
                    <h3 id="counter">0</h3>
                    <h4>SATIESFIED CUSTOMERS</h4>
				</div>
				<div class="col-md-3 experience-item">
					<i class="fas fa-trophy icon-experience"></i>
					<h3 id="counter1">0</h3>
					<h4>YEARS EXPERIENCE</h4>
				</div>
				<div class="col-md-3 experience-item">
					<i class="fas fa-home icon-experience"></i>
					<h3 id="counter2">0</h3>
					<h4>TRAINING ROOM</h4>
				</div>
				<div class="col-md-3 experience-item">
					<i class="fas fas fa-dumbbell icon-experience"></i>
					<h3 id="counter3">0</h3>
					<h4>GOLD MEDALS</h4>
				</div>
			</div>
		</div>
    </section>
    <!-- Home Page Experience Section End Here -->
     <!-- Home Trainer Area Start Here -->
     <div class="home-trainer-area">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">                         
                         <div class="section-title">
                             <h2>OUR <span>TRAINERS</span></h2>
                             <hr class="hr-center">
                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ut nuncsapien.Pellentesque feugiat risus nec fringilla dolor pellentesque aliquet.</p>
                         </div>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                         
                         <div class="team-area">
                            <div class="single-team">
                                <div class="images">
                                    <a href="#"><img src="img/trainers/1.jpg" alt=""></a>
                                    <div class="overley">
                                        <h3><a href="#">Jasmine Core</a></h3>
                                        <div class="others">
                                            <h4>Fitness Expert</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Re mihi non aeque satisfacit, et quidem locis pluribus.</p>
                                        </div>
                                    </div>
                                    <div class="social-media">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-team">
                                <div class="images">
                                    <a href="#"><img src="img/trainers/2.jpg" alt=""></a>
                                    <div class="overley">
                                        <h3><a href="#">Robert Herry</a></h3>
                                        <div class="others">
                                            <h4>Body Builder</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Re mihi non aeque satisfacit, et quidem locis pluribus.</p>
                                        </div>
                                    </div>
                                    <div class="social-media">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-team">
                                <div class="images">
                                    <a href="#"><img src="img/trainers/3.jpg" alt=""></a>                  
                               
                                    <div class="overley">
                                        <h3><a href="#">Jhon Mark</a></h3>
                                        <div class="others">
                                            <h4>Fitness Assistance</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Re mihi non aeque satisfacit, et quidem locis pluribus.</p>
                                        </div>
                                    </div>
                                    <div class="social-media">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-team">
                                <div class="images">
                                    <a href="#"><img src="img/trainers/4.jpg" alt=""></a>                  
                               
                                    <div class="overley">
                                        <h3><a href="#">Rock Swat</a></h3>
                                        <div class="others">
                                            <h4>Body Builder</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Re mihi non aeque satisfacit, et quidem locis pluribus.</p>
                                        </div>
                                    </div>
                                    <div class="social-media">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> 
                            <div class="single-team">
                                <div class="images">
                                    <a href="#"><img src="img/trainers/5.jpg" alt=""></a>                  
                               
                                    <div class="overley">
                                        <h3><a href="#">MR Merry </a></h3>
                                        <div class="others">
                                            <h4>Body Builder</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Re mihi non aeque satisfacit, et quidem locis pluribus.</p>
                                        </div>
                                    </div>
                                    <div class="social-media">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-team">
                                <div class="images">
                                    <a href="#"><img src="img/trainers/6.jpg" alt=""></a>                  
                               
                                    <div class="overley">
                                        <h3><a href="#">David Shan</a></h3>
                                        <div class="others">
                                            <h4>Body Builder</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Re mihi non aeque satisfacit, et quidem locis pluribus.</p>
                                        </div>
                                    </div>
                                    <div class="social-media">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>                            
                         </div>
                     </div>
                 </div>
             </div>
        </div>
        <!-- Home Trainer Area End Here -->
    

<?php require_once 'templates-home/footer.php';?>   





