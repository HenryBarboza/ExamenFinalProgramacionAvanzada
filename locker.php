<?php require_once 'templates/header.php';?>
<?php require_once 'config.php'; ?>

        <!-- Inicio Banner -->
        <section class="banner-locker">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1> <b>"NEW WEEK <span class="span">NEW GOALS"</b> </span></h1>
                        <p>Look in the mirror, that's your competition</p>
                    </div>
            </div>
        </div>
     </section>
    <!-- Fin Banner -->

     <div class="container course">
        <div class="table-wrapper">

            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h3>MANAGE  <b>LOCKERS</b> </h3>
					</div>
					
                </div>
			</div>
			
			<div class="col-sm-8">
						<a href="#addLockerModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add new locker</span></a>
			</div>

			<div class='col-sm-4 pull-right'>
				<div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control" placeholder="Search locker"  id="q" onkeyup="load(1);" />
                                <span class="input-group-btn">
                                    <button class="btn btn-info" type="button" onclick="load(1);">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                </div>
			</div>

			<div class='clearfix'></div>
            <hr>

            <?php require_once 'templates/message.php';?>
			<div id="loader_lockers"></div><!-- Carga de datos ajax aqui -->
			<div id="resultados"></div><!-- Carga de datos ajax aqui -->
            <div class='outer_div_lockers'></div><!-- Carga de datos ajax aqui -->
            
			
        </div>
	</div>
	
	

    <?php include("html/modal_locker/modal_add.php");?>
    <?php include("html/modal_locker/modal_edit.php");?>
    <?php include("html/modal_locker/modal_delete.php");?>
    
    <script src="js/locker_script.js"></script>



	
<?php require_once 'templates/footer.php';?>
