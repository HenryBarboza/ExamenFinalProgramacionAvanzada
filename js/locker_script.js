$(function() {
    load(1);
});
function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_lockers").fadeIn('slow');
    $.ajax({
        url:'ajax/locker_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_lockers").html("Loading...");
      },
        success:function(data){
            $(".outer_div_lockers").html(data).fadeIn('slow');
            $("#loader_lockers").html("");
        }
    })
}

$('#editLockerModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var code = button.data('code') 
  $('#edit_code').val(code)
  var nro = button.data('nro') 
  $('#edit_nro_locker').val(nro)

})

$('#deleteLockerModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var id = button.data('id') 
  $('#delete_id').val(id)
})


$( "#edit_locker" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/locker_edit.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#editLockerModal').modal('hide');
          }
    });
  event.preventDefault();
});


$( "#add_locker" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/save_locker.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#addLockerModal').modal('hide');
          }
    });
  event.preventDefault();
});

$( "#delete_locker" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/locker_delete.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#deleteLockerModal').modal('hide');
          }
    });
  event.preventDefault();
});

$(function() {
    load(1);
});

function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_lockers").fadeIn('slow');
    $.ajax({
        url:'ajax/locker_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_lockers").html("Loading...");
      },
        success:function(data){
            $(".outer_div_lockers").html(data).fadeIn('slow');
            $("#loader_lockers").html("");
        }
    })
}



