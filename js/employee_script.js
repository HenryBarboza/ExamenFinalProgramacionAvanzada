$(function() {
    load(1);
});
function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_employee").fadeIn('slow');
    $.ajax({
        url:'ajax/employee_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_employees").html("Loading...");
      },
        success:function(data){
            $(".outer_div_employees").html(data).fadeIn('slow');
            $("#loader_employees").html("");
        }
    })
}


$( "#add_employee" ).submit(function( event ) {
    var parametros = $(this).serialize();
      $.ajax({
              type: "POST",
              url: "ajax/employee_save.php",
              data: parametros,
               beforeSend: function(objeto){
                  $("#resultados").html("Sending...");
                },
              success: function(datos){
              $("#resultados").html(datos);
              load(1);
              $('#addEmployeeModal').modal('hide');
            }
      });
    event.preventDefault();
  });

$('#editEmployeeModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var code = button.data('code') 
  $('#edit_id_employee').val(code)
  var ci= button.data('ci') 
  $('#edit_ci_employee').val(ci)
  var name= button.data('name') 
  $('#edit_name').val(name)
  var surnames= button.data('surnames') 
  $('#edit_surnames').val(surnames)
  var phone= button.data('phone') 
  $('#edit_phone').val(phone)
  var address= button.data('address') 
  $('#edit_address').val(address)
  var position= button.data('position') 
  $('#edit_position').val(position)

})

$( "#edit_employee" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/employee_edit.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#editEmployeeModal').modal('hide');
          }
    });
  event.preventDefault();
});


$('#deleteEmployeeModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var id = button.data('id') 
    $('#delete_id').val(id)
  })
  
  $( "#delete_employee" ).submit(function( event ) {
    var parametros = $(this).serialize();
      $.ajax({
              type: "POST",
              url: "ajax/employee_delete.php",
              data: parametros,
               beforeSend: function(objeto){
                  $("#resultados").html("Sending...");
                },
              success: function(datos){
              $("#resultados").html(datos);
              load(1);
              $('#deleteEmployeeModal').modal('hide');
            }
      });
    event.preventDefault();
  });

$(function() {
    load(1);
});

function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_employee").fadeIn('slow');
    $.ajax({
        url:'ajax/employee_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_employees").html("Loading...");
      },
        success:function(data){
            $(".outer_div_employees").html(data).fadeIn('slow');
            $("#loader_employees").html("");
        }
    })
}


