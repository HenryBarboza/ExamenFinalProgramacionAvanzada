$(function() {
    load(1);
});
function load(page){
  var query=$("#q").val();
  var per_page=10;
  var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
  $("#loader_customers").fadeIn('slow');
  $.ajax({
      url:'ajax/customer_list.php',
      data: parametros,
       beforeSend: function(objeto){
      $("#loader_customers").html("Loading...");
    },
      success:function(data){
          $(".outer_div_customers").html(data).fadeIn('slow');
          $("#loader_customers").html("");
      }
  })
}

$('#editCustomerModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var id = button.data('id') 
  $('#edit_code').val(id)
  var ci= button.data('ci') 
  $('#edit_ci_customer').val(ci)
  var name = button.data('name') 
  $('#edit_name').val(name)
  var surnames= button.data('surnames') 
  $('#edit_surnames').val(surnames)
  var email = button.data('email') 
  $('#edit_email').val(email)
  var plan = button.data('plan') 
  $('#edit_id_plan').val(plan)
  var locker = button.data('locker') 
  $('#edit_id_locker').val(locker)

})

$('#deleteCustomerModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var id = button.data('id') 
  $('#delete_id').val(id)
})


$( "#edit_customer" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/customer_edit.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#editCustomerModal').modal('hide');
          }
    });
  event.preventDefault();
});

$( "#delete_customer" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/customer_delete.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#deleteCustomerModal').modal('hide');
          }
    });
  event.preventDefault();
});

$(function() {
    load(1);
}); 

function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_customers").fadeIn('slow');
    $.ajax({
        url:'ajax/customer_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_customers").html("Loading...");
      },
        success:function(data){
            $(".outer_div_customers").html(data).fadeIn('slow');
            $("#loader_customers").html("");
        }
    })
}

/**------------------------------------------------------------------------------------**/
/**---------------------            ADD NEW  MEASURE              ---------------------**/
/**------------------------------------------------------------------------------------**/

$( "#add_measure" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/save_measure.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#addMeasuresModal').modal('hide');
          }
    });
  event.preventDefault();
});

/**--------------------------------------------------------------------------------------**/
/**------------------   LISTAR MEASURE, CON EVENTO CLICK         ---------------**/
/**--------------------------------------------------------------------------------------**/
function list_measures($id_customer, page){
  var query=$("#q").val();
  var per_page=10;
  var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page, 'id_customer':$id_customer};
  $("#loader_measures").fadeIn('slow');
  $.ajax({
      url:'ajax/measure_list.php',
      data: parametros,
       beforeSend: function(objeto){
      $("#loader_measures").html("Loading...");
    },
      success:function(data){
          $(".outer_div_measures").html(data).fadeIn('slow');
          $("#loader_measures").html("");
      }
  })
}  



