$(function() {
    load(1);
});
function load(page){
  var query=$("#q").val();
  var per_page=10;
  var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
  $("#loader_plans").fadeIn('slow');
  $.ajax({
      url:'ajax/plan_list.php',
      data: parametros,
       beforeSend: function(objeto){
      $("#loader_plans").html("Loading...");
    },
      success:function(data){
          $(".outer_div_plans").html(data).fadeIn('slow');
          $("#loader_plans").html("");
      }
  })
}

$('#editPlanModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var code = button.data('code') 
  $('#edit_code').val(code)
  var name = button.data('name') 
  $('#edit_name').val(name)
  var time= button.data('price') 
  $('#edit_price').val(time)

})

$('#deletePlanModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var id = button.data('id') 
  $('#delete_id').val(id)
})


$( "#edit_plan" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/plan_edit.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#editPlanModal').modal('hide');
          }
    });
  event.preventDefault();
});

$( "#delete_plan" ).submit(function( event ) {
  var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "ajax/plan_delete.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html("Sending...");
              },
            success: function(datos){
            $("#resultados").html(datos);
            load(1);
            $('#deletePlanModal').modal('hide');
          }
    });
  event.preventDefault();
});

$(function() {
    load(1);
});

function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_plans").fadeIn('slow');
    $.ajax({
        url:'ajax/plan_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_plans").html("Loading...");
      },
        success:function(data){
            $(".outer_div_plans").html(data).fadeIn('slow');
            $("#loader_plans").html("");
        }
    })
}



