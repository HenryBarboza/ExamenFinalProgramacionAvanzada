(function ($) {
    "use strict";


     $(".team-area").owlCarousel({
         autoPlay: true, 
         slideSpeed:1000,
         pagination:false,
         navigation:true,	  
         items : 3,
         navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
         itemsDesktop : [1199,3],
         itemsDesktopSmall : [992,2],
         itemsTablet: [768,2],
         itemsMobile : [480,1],
     });
   
    $(".about-trainer-slide").owlCarousel({
         autoPlay: true, 
         slideSpeed:1000,
         pagination:false,
         navigation:true,    
           items : 4,
         navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
           itemsDesktop : [1199,3],
         itemsDesktopSmall : [992,2],
         itemsTablet: [768,1],
         itemsMobile : [480,1],
    });    
    
})(jQuery); 