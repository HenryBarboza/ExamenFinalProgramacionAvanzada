$(function() {
    load(1);
});
function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_coachs").fadeIn('slow');
    $.ajax({
        url:'ajax/coach_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_coachs").html("Loading...");
      },
        success:function(data){
            $(".outer_div_coachs").html(data).fadeIn('slow');
            $("#loader_coachs").html("");
        }
    })
}

$( "#add_coach" ).submit(function( event ) {
    var parametros = $(this).serialize();
      $.ajax({
              type: "POST",
              url: "ajax/coach_save.php",
              data: parametros,
               beforeSend: function(objeto){
                  $("#resultados").html("Sending...");
                },
              success: function(datos){
              $("#resultados").html(datos);
              load(1);
              $('#addCoachModal').modal('hide');
            }
      });
    event.preventDefault();
  });
  
$('#editCoachModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var code = button.data('code') 
    $('#edit_id_coach').val(code)
    var ci= button.data('ci') 
    $('#edit_ci_coach').val(ci)
    var name= button.data('name') 
    $('#edit_name').val(name)
    var surnames= button.data('surnames') 
    $('#edit_surnames').val(surnames)
    var phone= button.data('phone') 
    $('#edit_phone').val(phone)
    var address= button.data('address') 
    $('#edit_address').val(address)
    var profession= button.data('profession') 
    $('#edit_profession').val(profession)
  
  })
  
  $( "#edit_coach" ).submit(function( event ) {
    var parametros = $(this).serialize();
      $.ajax({
              type: "POST",
              url: "ajax/coach_edit.php",
              data: parametros,
               beforeSend: function(objeto){
                  $("#resultados").html("Sending...");
                },
              success: function(datos){
              $("#resultados").html(datos);
              load(1);
              $('#editCoachModal').modal('hide');
            }
      });
    event.preventDefault();
  });


  $('#deleteCoachModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var id = button.data('id') 
    $('#delete_id').val(id)
  })
  
  $( "#delete_coach" ).submit(function( event ) {
    var parametros = $(this).serialize();
      $.ajax({
              type: "POST",
              url: "ajax/coach_delete.php",
              data: parametros,
               beforeSend: function(objeto){
                  $("#resultados").html("Sending...");
                },
              success: function(datos){
              $("#resultados").html(datos);
              load(1);
              $('#deleteCoachModal').modal('hide');
            }
      });
    event.preventDefault();
  });



$(function() {
    load(1);
});

function load(page){
    var query=$("#q").val();
    var per_page=10;
    var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
    $("#loader_coachs").fadeIn('slow');
    $.ajax({
        url:'ajax/coach_list.php',
        data: parametros,
         beforeSend: function(objeto){
        $("#loader_coachs").html("Loading...");
      },
        success:function(data){
            $(".outer_div_coachs").html(data).fadeIn('slow');
            $("#loader_coachs").html("");
        }
    })
}



